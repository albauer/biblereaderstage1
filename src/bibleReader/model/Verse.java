package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @version 2019.2.6
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Alexander Bauer (Implementation)
 */
public class Verse implements Comparable<Verse> {

	// TODO Add Fields here. (There should be 2 of them--one for the reference
	// and one for the text of the verse.)

	private Reference reference;
	private String text;

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param r
	 *            The reference for the verse
	 * @param t
	 *            The text of the verse
	 */
	public Verse(Reference r, String t) {
		reference = r;
		text = t;
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book
	 *            The book of the Bible
	 * @param chapter
	 *            The chapter number
	 * @param verse
	 *            The verse number
	 * @param text
	 *            The text of the verse
	 */
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		reference = new Reference(book, chapter, verse);
		this.text = text;
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		return reference;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of
	 * the text of the verse.
	 */
	@Override
	public String toString() {
		return reference.toString() + " " + text;
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Verse) {
			Verse verse = (Verse) other;
			if (text.equals(verse.getText()) && reference.equals(verse.getReference())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * A hash code numerical representation of the object
	 */
	@Override
	public int hashCode() {
		// TODO Implement me: Stage 2
		// Don't forget the rules for how equals and hashCode are supposed to be
		// related.
		return toString().hashCode();
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to
	 * work.
	 * 
	 * @param verse
	 *            The verse object to compare this verse object to
	 * @return negative number if the verse it is compared to is after this
	 *         verse zero if zero the two verses are equal positive number if
	 *         the verse it is compared to is before this verse
	 */
	@Override
	public int compareTo(Verse other) {
		if (reference.compareTo(other.getReference()) != 0) {
			return reference.compareTo(other.getReference());
		} else if (text.compareTo(other.getText()) != 0) {
			return text.compareTo(other.getText());
		} else {
			return 0;
		}
	}

	/**
	 * Return true if and only if this verse the other verse have the same
	 * reference. (So the text is ignored).
	 * 
	 * @param other
	 *            the other Verse.
	 * @return true if and only if this verse and the other have the same
	 *         reference.
	 */
	public boolean sameReference(Verse other) {
		return getReference().equals(other.getReference());
	}

}
