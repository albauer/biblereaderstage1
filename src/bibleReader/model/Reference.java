package bibleReader.model;

// TODO Add Javadoc comments throughout the class.

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @version 2019.2.6
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Alexander Bauer (Implementation)
 */
public class Reference implements Comparable<Reference> {

	/*
	 * Add the appropriate fields and implement the methods. Notice that there
	 * are no setters. This is intentional since a reference shouldn't change.
	 * Important: You should NOT store the book as a string. There are several
	 * reasons for this that will be clear if you think about it for a few
	 * minutes. There is already a class that you can use instead.
	 */

	// TODO Add fields: Stage 2
	private BookOfBible book;
	private int chapter;
	private int verse;

	/**
	 * Creates a new reference object for a location in the bible
	 * 
	 * @param book
	 *            The book of the bible
	 * @param chapter
	 *            The chapter of the book
	 * @param verse
	 *            The verse number within the chapter
	 */
	public Reference(BookOfBible book, int chapter, int verse) {
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	/**
	 * Returns the book, as a string
	 * 
	 * @return The book, as a string
	 */
	public String getBook() {
		return book.toString();
	}

	/**
	 * Returns the book of this object, as a BookOfBible object
	 * 
	 * @return The book this object holds, as a BookOfBible object
	 */
	public BookOfBible getBookOfBible() {
		return book;
	}

	/**
	 * Returns the chapter of this object (int)
	 * 
	 * @return The chapter of this object
	 */
	public int getChapter() {
		return chapter;
	}

	/**
	 * Returns the verse number of this object (int)
	 * 
	 * @return The verse of this object
	 */
	public int getVerse() {
		return verse;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse"
	 * For instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		return book.toString() + " " + chapter + ":" + verse;
	}

	/**
	 * Tests whether two objects are equal (at least one of which is a
	 * reference) Return true if and only if the chapter, book, and verse are
	 * all the same.
	 * 
	 * @param other
	 *            The object to compare
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Reference) {
			Reference reference = (Reference) other;
			if ((chapter == reference.getChapter()) && (verse == reference.getVerse())
					&& (book == reference.getBookOfBible())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @return A hash code numerical representation of this object
	 */
	@Override
	public int hashCode() {
		// I'll give you this one as a freebie. This is a trick I learned from
		// Dr. McFall.
		// If two instances are the same, their toString method will return the
		// same thing (assuming you implement toString properly).
		// Thus calling hashCode on them will return the same int, so we are
		// honoring the contract that if a.equals(b) then
		// a.hashCode==b.hashCode.
		// Why go to extra trouble to implement hashCode based on the fields and
		// some weird formula when String already has an implementation?
		// (Well, maybe because it is perhaps less efficient, but we'll do it
		// anyway since it probably isn't that bad.)
		return toString().hashCode();
	}

	/**
	 * 
	 * @param otherRef
	 *            The other reference to compare to
	 * @return negative number if the reference it is compared to is after this
	 *         reference zero if zero the two reference are equal positive
	 *         number if the reference it is compared to is before this
	 *         reference
	 */
	@Override
	public int compareTo(Reference otherRef) {

		if ((book.compareTo(otherRef.getBookOfBible())) != 0) {
			return book.compareTo(otherRef.getBookOfBible());
		} else if ((chapter - otherRef.getChapter()) != 0) {
			return chapter - otherRef.getChapter();
		} else if ((verse - otherRef.getVerse()) != 0) {
			return verse - otherRef.getVerse();
		}
		return 0;
	}
}
