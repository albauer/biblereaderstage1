package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * 
 * // Make sure you test at least the following:
 * --All of the constructors and all of the methods.   -- DONE, in testEquals()
 * --Using a full title or an abbreviation for a book name.   -- DONE
 * --The equals for success and for failure, especially when they match in 2 of the 3 places   -- DONE
 *    (e.g. same book and chapter but different verse, same chapter and verse but different book, etc.)
 * --That compareTo is ordered properly for similar cases as the equals tests.   -- DONE
 *    (e.g. should Genesis 1:1 compareTo Revelation 22:21 return a positive or negative number?)
 * --Any other potentially tricky things.   -- Well, I can't think of any, can you?? (Hint: No)
 * 
 * @author Alexander Bauer and Angel Iniguez
 * @version 2019.2.3
 */



public class Stage01StudentReferenceTest {

	// A few sample references to get you started. Will have six total
	private Reference	ruth2_4;
	private Reference	gen1_3;
	private Reference	rev5_12;
	private Reference   mark6_5;
	private Reference   psalms130_1;
	private Reference   exodus7_2;
	private Reference   ruth2_4_2;  //Used in testEquals 
	private Reference   gen1_3_2;   //Used in testEquals
	private Reference   rev5_13;
	private Reference   mark5_7;
	private Reference   exodus1_3;
	
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 * Acts like the constructor for the test class
	 */
	@Before
	public void setUp() throws Exception {
		// A few sample references to get you started.
		ruth2_4 = new Reference(BookOfBible.Ruth, 2, 4);
		gen1_3 = new Reference(BookOfBible.Genesis, 1, 3);
		rev5_12 = new Reference(BookOfBible.Revelation, 5, 12);
		mark6_5 = new Reference(BookOfBible.Mark, 6, 5);
		psalms130_1 = new Reference(BookOfBible.Psalms, 130, 1);
		exodus7_2 = new Reference(BookOfBible.Exodus, 7, 2);
		ruth2_4_2 = new Reference(BookOfBible.Ruth, 2, 4);
		gen1_3_2 = new Reference(BookOfBible.Genesis, 1, 3);
		rev5_13 = new Reference(BookOfBible.Revelation, 5, 13);
		mark5_7 = new Reference(BookOfBible.Mark, 5, 7);
		exodus1_3 = new Reference(BookOfBible.Exodus, 1, 3);
		
		// TODO Create more objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.   -- DONE
	}

	
	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}
	
	@Test
	public void testReference() {
		
	}

	/**
	 * Comment added by Cusack at very beginning
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	
	/**
	 * The equals method returns a boolean, so need to do assertTrue
	 * Constructor also tested here, see 3.b.5 on assignment sheet
	 */
	@Test
	public void testEquals() {
		// Test the equalities -- Everything should be equal here
		assertTrue(ruth2_4.equals(ruth2_4_2));
		assertTrue(gen1_3.equals(gen1_3_2));
		
		// Test things that should be equal EXCEPT for one part
		assertFalse(rev5_12.equals(rev5_13));
		assertFalse(mark6_5.equals(mark5_7));
		assertFalse(gen1_3.equals(exodus1_3));
		
		// Test things that are completely different
		assertFalse(ruth2_4.equals(psalms130_1));
		assertFalse(mark5_7.equals(ruth2_4_2));
	}
	
	/**
	 * Test the getters in the Reference class. We will test each getter method with 
	 * two of the fields.
	 */
	@Test
	public void testGetters() {
		// Testing getBook()
		assertEquals(ruth2_4.getBook(), "Ruth");
		assertEquals(gen1_3.getBook(), "Genesis");
		// Testing getBookOfBible()
		assertEquals(rev5_12.getBookOfBible(), BookOfBible.Revelation);
		assertEquals(mark6_5.getBookOfBible(), BookOfBible.Mark);
		
		// Testing getBookOfBible using the abbreviations
		assertEquals(rev5_12.getBookOfBible(), BookOfBible.getBookOfBible("rv"));
		assertEquals(mark6_5.getBookOfBible(), BookOfBible.getBookOfBible("mk"));
		
		// Testing getChapter()
		assertEquals(psalms130_1.getChapter(), 130);
		assertEquals(exodus7_2.getChapter(), 7);
		// Test getVerse()
		assertEquals(ruth2_4.getVerse(), 4);
		assertEquals(mark6_5.getVerse(), 5);		
	}
	
	/**
	 * Method hashCode in Reference returns an int. Making use of how the hashCode is generated.
	 * We don't need any more than two.
	 */
	@Test
	public void testHashCode() {
		assertEquals(ruth2_4.hashCode(), ruth2_4.toString().hashCode());
		assertEquals(mark6_5.hashCode(), mark6_5.toString().hashCode());
	}
	
	/**
	 * Test the toString method in the Reference class. Testing using four of the fields declared above
	 */
	@Test
	public void testToString() {
		assertEquals(ruth2_4.toString(), "Ruth 2:4");
		assertEquals(gen1_3.toString(), "Genesis 1:3");
		assertEquals(rev5_12.toString(), "Revelation 5:12");
		assertEquals(mark6_5.toString(), "Mark 6:5");
	}

	/**
	 * Test the compareTo method in the Reference class, it compares when the first book is before
	 * the second, second is before the first, and both books are equal
	 */
	@Test
	public void testcompareTo() {
		// Genesis 1:3 is closer to the front of the bible. Psalsm is 18 books away
		// In total, testing two where the first is closer to the front than the second
		assertEquals(gen1_3.compareTo(psalms130_1), -18);
		assertEquals(ruth2_4.compareTo(mark5_7), -33);
		// Test two that are exactly the same
		assertEquals(ruth2_4.compareTo(ruth2_4_2), 0);
		assertEquals(gen1_3.compareTo(gen1_3_2), 0);
		// Test two where the first is farther in the back, should return positive values
		assertEquals(rev5_12.compareTo(exodus1_3), 65);
		assertEquals(psalms130_1.compareTo(ruth2_4), 11);
	}
}
