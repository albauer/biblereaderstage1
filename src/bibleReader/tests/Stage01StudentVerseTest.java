package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;

/*
 * Tests for the Verse class
 * 
 * @author Alexander Bauer and Angel Iniguez Jr.
 * @version 2019.2.4
 */

public class Stage01StudentVerseTest {

	private Reference ruth2_4;
	private Reference gen1_3;
	private Reference rev5_12;
	private Reference mark6_5;
	private Reference psalms130_1;
	private Reference ruth2_4_2; // Used in testEquals
	private Reference gen1_3_2; // Used in testEquals
	private Reference exodus1_3;

	private Verse ruth2_4Verse;
	private Verse gen1_3Verse;
	private Verse rev5_12Verse;
	private Verse mark6_5Verse;
	private Verse psalms130_1Verse;
	private Verse ruth2_4Dummy;
	private Verse gen1_3Dummy;
	private Verse ruth2_4_2Verse;
	private Verse gen1_3_2Verse;
	private Verse exodus1_3Verse;

	/**
	 * Acts as the constructor
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		ruth2_4 = new Reference(BookOfBible.Ruth, 2, 4);
		gen1_3 = new Reference(BookOfBible.Genesis, 1, 3);
		rev5_12 = new Reference(BookOfBible.Revelation, 5, 12);
		mark6_5 = new Reference(BookOfBible.Mark, 6, 5);
		psalms130_1 = new Reference(BookOfBible.Psalms, 130, 1);
		exodus1_3 = new Reference(BookOfBible.Exodus, 1, 3);

		ruth2_4Verse = new Verse(ruth2_4,
				"Just then Boaz arrived from Bethlehem and greeted the harvesters, 'The Lord be with you!' 'The Lord bless you!' they answered.");
		gen1_3Verse = new Verse(gen1_3, "And God said, 'Let there be light,' and there was light.");
		rev5_12Verse = new Verse(rev5_12,
				"In a loud voice they were saying: 'Worthy is the Lamb, who was slain, to receive power and wealth and wisdom and strength and honor and glory and praise!'");
		mark6_5Verse = new Verse(mark6_5,
				"He could not do any miracles there, except lay his hands on a few sick people and heal them.");
		psalms130_1Verse = new Verse(psalms130_1, "Out of the depths I cry to you, Lord");
		ruth2_4Dummy = new Verse(ruth2_4, "Ruth 2:4 DUMMY");
		gen1_3Dummy = new Verse(gen1_3, "Genesis 1:3 DUMMY");
		exodus1_3Verse = new Verse(exodus1_3, "Issachar, Zebulun and Benjamin");
	}

	/*
	 * Anything that should be done at the end of each test. Most likely you
	 * won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions
	 * (without implementation) are given below.
	 */

	/**
	 * Test each getter (accessor) twice
	 */
	@Test
	public void testGetters() {
		// Testing getReference
		assertEquals(ruth2_4Verse.getReference(), ruth2_4);
		assertEquals(gen1_3Verse.getReference(), gen1_3);
		// Testing getText -- ideally wouldn't reuse verses, but want to use
		// short verses
		assertEquals(gen1_3Verse.getText(), "And God said, 'Let there be light,' and there was light.");
		assertEquals(psalms130_1Verse.getText(), "Out of the depths I cry to you, Lord");
	}

	/**
	 * Testing the toString representation of Verse Combine the Reference
	 * toString with the Verse's getText methods
	 */
	@Test
	public void testToString() {
		assertEquals(rev5_12Verse.toString(), rev5_12.toString() + " " + rev5_12Verse.getText());
		assertEquals(mark6_5Verse.toString(), mark6_5.toString() + " " + mark6_5Verse.getText());
	}

	/**
	 * Testing the .equals method, overriden in Verse Also tests the
	 * constructors (of which there are two)
	 */
	@Test
	public void testEquals() {
		// Test second constructor
		ruth2_4_2Verse = new Verse(BookOfBible.Ruth, 2, 4,
				"Just then Boaz arrived from Bethlehem and greeted the harvesters, 'The Lord be with you!' 'The Lord bless you!' they answered.");
		gen1_3_2Verse = new Verse(BookOfBible.Genesis, 1, 3,
				"And God said, 'Let there be light,' and there was light.");
		// Test equality of contents of Verse objects
		// First, test that the references are the same
		assertEquals(ruth2_4Verse.getReference(), ruth2_4_2Verse.getReference());
		assertEquals(gen1_3Verse.getReference(), gen1_3_2Verse.getReference());
		// Second, test that the text of the verse is the same
		assertEquals(ruth2_4Verse.getText(), ruth2_4_2Verse.getText());
		assertEquals(gen1_3Verse.getText(), gen1_3_2Verse.getText());
		// Test some cases where it shouldn't pass
		assertEquals(ruth2_4Verse.getReference(), ruth2_4Dummy.getReference());
		assertNotEquals(ruth2_4Verse.getText(), ruth2_4Dummy.getText());
	}

	/**
	 * Tests the hash code representation of a Verse
	 */
	@Test
	public void testHashCode() {
		assertEquals(mark6_5Verse.hashCode(), mark6_5Verse.toString().hashCode());
		assertEquals(psalms130_1Verse.hashCode(), psalms130_1Verse.toString().hashCode());
	}

	@Test
	public void testCompareTo() {
		// Genesis 1:3 is closer to the front of the bible. Psalsm is 18 books
		// away
		// In total, testing two where the first is closer to the front than the
		// second
		assertEquals(gen1_3Verse.compareTo(psalms130_1Verse), -18);
		assertEquals(ruth2_4Verse.compareTo(mark6_5Verse), -33);
		// Test two that are exactly the same
		assertEquals(ruth2_4Verse.compareTo(ruth2_4_2Verse), 0);
		assertEquals(gen1_3Verse.compareTo(gen1_3_2Verse), 0);
		// Test two where the first is farther in the back, should return
		// positive values
		assertEquals(rev5_12Verse.compareTo(exodus1_3Verse), 65);
		assertEquals(psalms130_1Verse.compareTo(ruth2_4Verse), 11);
	}

	/**
	 * Test sameReference. Test once where the references are the same but the
	 * verses are not, and one where the references and text and completely
	 * different. Each case is repeated twice.
	 */
	@Test
	public void testSameReference() {
		// Test two that have same reference, but are given different text
		assertTrue(ruth2_4Verse.sameReference(ruth2_4Dummy));
		assertTrue(gen1_3Verse.sameReference(gen1_3Dummy));
		// Test two that are completely different
		assertFalse(rev5_12Verse.sameReference(mark6_5Verse));
		assertFalse(psalms130_1Verse.sameReference(mark6_5Verse));
	}

}
